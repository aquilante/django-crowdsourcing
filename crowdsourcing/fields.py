try:
    from sorl.thumbnail.fields import ImageWithThumbnailsField
except ImportError:

    from django.db.models import ImageField


    class ImageWithThumbnailsField(ImageField):
        def __init__(self, *args, **kwargs):
            self.thumbnail = kwargs.pop('thumbnail', None)
            self.extra_thumbnails = kwargs.pop('extra_thumbnails', None)
            super(ImageWithThumbnailsField, self).__init__(*args, **kwargs)

    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([
        (
            [ImageWithThumbnailsField], # Class(es) these apply to
            [],         # Positional arguments (not used)
            {           # Keyword argument
                "thumbnail": ["thumbnail", {}],
                "extra_thumbnails":["extra_thumbnails", {}]
            },
                                        ),
        ], ["^crowdsourcing\.fields\.ImageWithThumbnailsField"])


