from __future__ import absolute_import

from django.http import Http404

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import routers
from rest_framework import filters

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.decorators import *

from .models import *

import collections





class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey


class SurveyViewSet(viewsets.ModelViewSet):
    model = Survey
    serializer_class = SurveySerializer


    def get_survey(self, pk):
        try:
            return Survey.objects.get(pk=pk)
        except Survey.DoesNotExist:
            raise Http404


    @link()
    def answers(self, request, pk=None):
        if pk is None:
            return Response()
        survey = self.get_survey(pk)
        submission_ids = survey.submission_set.values_list('id', flat=True)
        answers = Answer.objects.filter(submission__in=submission_ids)
        serializer = AnswerSerializer(answers, many=True)
        
        return Response(serializer.data)

    @link()
    def stats(self, request, pk=None):
        survey = self.get_survey(pk)
        stats = {}
        for q in survey.questions.all():
            if q.option_type == OPTION_TYPE_CHOICES.LOCATION:
                fields = ('latitude', 'longitude')
                return Response()
            
            if q.option_type == OPTION_TYPE_CHOICES.INTEGER:
                field = 'integer_answer'
            elif q.option_type == OPTION_TYPE_CHOICES.FLOAT:
                field = 'float_answer'
            elif q.option_type == OPTION_TYPE_CHOICES.BOOL:
                field = 'boolean_answer'
            elif q.option_type == OPTION_TYPE_CHOICES.SELECT:
                field = 'text_answer'
            elif q.option_type == OPTION_TYPE_CHOICES.CHOICE:
                field = 'text_answer'
            elif q.option_tyoe == OPTION_TYPE_CHOICES.NUMERIC_SELECT:
                field = 'integer_answer'
            elif q.option_type == OPTION_TYPE_CHOICES.NUMERIC_CHOICE:
                field = 'integer_answer'
            else:
                return Repsonse()

            answers = Answer.objects.filter(question=q).values_list(field, flat=True)
            stats[q.label] = collections.Counter(answers)
        return Response(stats)
            


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question


class QuestionViewSet(viewsets.ModelViewSet):
    model = Question
    serializer_class = QuestionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('id', 'survey', 'answer_is_public',)

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer


class AnswerViewSet(viewsets.ModelViewSet):
    model = Answer
    serializer_class = AnswerSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('id', 'submission', 'question',)

class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission


class SubmissionViewSet(viewsets.ModelViewSet):
    model = Submission
    serializer_class = SubmissionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('id', 'survey', 'is_public',)


router = routers.DefaultRouter()
router.register(r'surveys', SurveyViewSet)
router.register(r'questions', QuestionViewSet)
router.register(r'answers', AnswerViewSet)
router.register(r'submissions', SubmissionViewSet)


